package org.bitbucket.charlbrink.trigger;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Endpoint(id="job")
public class JobTriggerEndpoint {

    @WriteOperation
    public void run(@Selector String jobName, String triggeredBy) {
        log.info("Actuator endpoint job {} triggered by {}", jobName, triggeredBy);
    }
}
